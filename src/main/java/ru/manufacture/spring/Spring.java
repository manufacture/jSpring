package ru.manufacture.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class Spring {
    private static class Single {
        private static Spring spring = new Spring();
    }

    private ApplicationContext context = null;

    private Spring() {
        context = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext*.xml");
    }

    public static ApplicationContext context(){
        return Single.spring.context;
    }

    public static boolean contains(String beanName) {
        return Single.spring.context.containsBean(beanName);
    }

    public static <Type> Type bean(String name) {
        return (Type) context().getBean(name);
    }
}
