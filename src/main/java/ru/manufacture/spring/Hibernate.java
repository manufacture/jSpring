package ru.manufacture.spring;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class Hibernate {
    @Autowired
    @Qualifier("sessionFactory")
    protected SessionFactory sessionFactory;

    public Session session() {
        try {
            return sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            return sessionFactory.openSession();
        }
    }

    public static Hibernate instance() {
        Hibernate hibernate = new Hibernate();
        hibernate.sessionFactory = Spring.bean("sessionFactory");
        return hibernate;
    }
}
