package ru.manufacture.spring;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * @author Degtyarev Roman
 * @date 27.08.2015.
 */
public abstract class Transaction {
    private TransactionStatus status;
    private DefaultTransactionDefinition transactionDefinition;

    protected abstract PlatformTransactionManager getTransactionManager();

    public void begin(int propagation, int isolation, int timeout) {
        status = getTransactionManager().getTransaction(getDefinition(propagation, isolation, timeout));
    }

    public void begin(int propagation, int isolation) {
        begin(propagation, isolation, TransactionDefinition.TIMEOUT_DEFAULT);
    }

    public void begin(int propagation) {
        begin(propagation, TransactionDefinition.ISOLATION_DEFAULT);
    }

    public void begin() {
        begin(TransactionDefinition.PROPAGATION_REQUIRED);
    }

    public void commit() {
        getTransactionManager().commit(getStatus());
    }

    public void rollback() {
        getStatus().setRollbackOnly();
        getTransactionManager().rollback(getStatus());
    }

    public TransactionStatus getStatus() {
        return status;
    }

    private DefaultTransactionDefinition getDefinition(int propagation, int isolation, int timeout) {
        if (null == transactionDefinition) {
            return resetDefinition(propagation, isolation, timeout);
        }
        if (notEquals(transactionDefinition, propagation, isolation, timeout)) {
            return resetDefinition(propagation, isolation, timeout);
        }
        return transactionDefinition;
    }

    private boolean notEquals(DefaultTransactionDefinition tf, int propagation, int isolation, int timeout) {
        return propagation != tf.getPropagationBehavior() ||
                isolation != tf.getIsolationLevel() ||
                timeout != tf.getTimeout();
    }

    private DefaultTransactionDefinition resetDefinition(int propagation, int isolation, int timeout) {
        transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(propagation);
        transactionDefinition.setIsolationLevel(isolation);
        transactionDefinition.setTimeout(timeout);
        return transactionDefinition;
    }
}
