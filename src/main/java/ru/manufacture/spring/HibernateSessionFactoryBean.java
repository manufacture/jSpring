package ru.manufacture.spring;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

/**
 * @author Degtyarev Roman
 * @date 23.10.2015.
 */
public class HibernateSessionFactoryBean implements FactoryBean<SessionFactory> {
    private String sessionFactoryBeanNameHolder;

    @Override
    public SessionFactory getObject() throws Exception {
        boolean sessionFactoryDefined = Spring.contains(sessionFactoryBeanNameHolder);
        if (sessionFactoryDefined) {
            String sessionFactoryBeanName = Spring.bean(sessionFactoryBeanNameHolder);
            return Spring.bean(sessionFactoryBeanName);
        }
        throw new NoSuchBeanDefinitionException("Session factory bean not defined: sessionFactoryBeanNameHolder");
    }

    @Override
    public Class<?> getObjectType() {
        return SessionFactory.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    public String getSessionFactoryBeanNameHolder() {
        return sessionFactoryBeanNameHolder;
    }

    public void setSessionFactoryBeanNameHolder(String sessionFactoryBeanNameHolder) {
        this.sessionFactoryBeanNameHolder = sessionFactoryBeanNameHolder;
    }
}
