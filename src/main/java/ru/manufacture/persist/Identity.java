/**
 * @author Degtyarev Roman
 * @date 16.09.2015.
 */
package ru.manufacture.persist;

/**
 * идентифицируемый объект
 */
public interface Identity <Type> {
    /**
     * @return идентификатор объекта
     */
    Type getId();
}
