package ru.manufacture.persist;

import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import ru.manufacture.spring.Hibernate;

/**
 * @author Degtyarev Roman
 * @date 29.01.2016.
 */
public class DeclarableDaoImpl extends Hibernate implements DeclarableDao {
    @Override
    public List<? extends Declarable> getDeclarable(Class<? extends Declarable> type, Collection<String> names) {
        Criteria criteria = session().createCriteria(type).add(Restrictions.in("name", names));
        return criteria.list();
    }

    @Override
    public <Type extends Declarable> Type getDeclarable(Class<Type> type, String name) {
        Criteria criteria = session().createCriteria(type).add(Restrictions.eq("name", name));
        return (Type) criteria.uniqueResult();
    }

    @Override
    public <Type extends Declarable> void saveDeclarable(Type declarable) {
        session().saveOrUpdate(declarable);
    }

    @Override
    public <Type extends Declarable> void mergeDeclarable(Type declarable) {
        session().merge(declarable);
    }
}
