/**
 * @author Degtyarev Roman
 * @date 16.09.2015.
 */
package ru.manufacture.persist;

/**
 * поименованный объект
 */
public interface Named {
    /**
     * @return возвращает имя объекта
     */
    String getName();
}
