package ru.manufacture.persist;

import java.util.Collection;
import java.util.List;

/**
 * @author Degtyarev Roman
 * @date 29.01.2016.
 */
public interface DeclarableDao {
    List<? extends Declarable> getDeclarable(Class<? extends Declarable> type, Collection<String> names);
    <Type extends Declarable> Type getDeclarable(Class<Type> type, String name);
    <Type extends Declarable> void saveDeclarable(Type declarable);
    <Type extends Declarable> void mergeDeclarable(Type declarable);
}
