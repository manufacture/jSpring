/**
 * @author Degtyarev Roman
 * @date 22.06.2015.
 */
package ru.manufacture.persist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * Декларируемый объект
 */
@MappedSuperclass
public class Declarable implements Serializable, Named, Identity<Long> {
    /**
     * идентификатор объекта
     */
    private long id;
    /**
     * имя объекта
     */
    private String name;
    /**
     * описание объекта
     */
    private String description;

    /**
     * @return возвращает идентификатор объекта
     */
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="jmanufacture_seq")
    @SequenceGenerator(name="jmanufacture_seq", sequenceName="jmanufacture_seq", allocationSize = 1)
    public Long getId() {
        return id;
    }

    /**
     * @return возвращает имя объекта
     */
    @Column(name = "name", nullable = false, unique = true)
    public String getName() {
        return name;
    }

    /**
     * @return возвращает описание объекта
     */
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    /**
     * @param id идентификатор объекта
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param name имя объекта
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param description описание объекта
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Declarable) {
            Declarable other = (Declarable) obj;
            return name.equals(other.name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * возвращает имена объектов
     * @param named коллекция поименованных объектов
     * @return возвращает коллекцию имен
     */
    @Transient
    public static Collection<String> getNames(Collection<? extends Named> named) {
        Collection<String> names = new ArrayList<>(named.size());
        for (Named item : named) {
            names.add(item.getName());
        }
        return names;
    }

    /**
     * возвращает идентификаторы объектов
     * @param identities коллекция идентифицируемых объектов
     * @return возвращает коллекцию идентификаторов
     */
    @Transient
    public static Collection<Long> getIds(Collection<? extends Identity<Long>> identities) {
        Collection<Long> ids = new ArrayList<>(identities.size());
        for (Identity<Long> item : identities) {
            ids.add(item.getId());
        }
        return ids;
    }
}
