package ru.manufacture.spring;

import java.util.Arrays;
import org.junit.Test;

public class SpringTest {

    @Test
    public void testBean() throws Exception {
        String[] names = Spring.context().getBeanDefinitionNames();
        System.out.println(Arrays.asList(names));
    }
}